﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            IPlayer player1 = new FITZV1(); // enter your AI here
            IPlayer player2 = new FearonBensonPlayer(); //enter opponent AI here

            Application.Run(new Form1(player1, player2));
        }
    }
}
