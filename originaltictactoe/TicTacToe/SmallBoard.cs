﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class SmallBoard
    {
        // Note: none = 0, player 1 = 1, player 2 = 2, draw = 3
        
        public bool isWon { get; set; }
        public int winner { get; set; }
        public int[] squares { get; set; }

        public SmallBoard()
        {
            isWon = false;
            winner = 0;
            squares = new int[9];
            for (int i = 0; i < squares.Length; i++)
            {
                squares[i] = 0;
            }
        }

        public void CheckForWinner()
        {
            if ((squares[0] != 0) && (squares[0] == squares[1]) && (squares[1] == squares[2]))
            {
                isWon = true;
                winner = squares[0];
            } // top row
            else if ((squares[3] != 0) && (squares[3] == squares[4]) && (squares[4] == squares[5]))
            {
                isWon = true;
                winner = squares[3];
            } // middle row
            else if ((squares[6] != 0) && (squares[6] == squares[7]) && (squares[7] == squares[8]))
            {
                isWon = true;
                winner = squares[6];
            } // bottom row
            else if ((squares[0] != 0) && (squares[0] == squares[3]) && (squares[3] == squares[6]))
            {
                isWon = true;
                winner = squares[0];
            } // left column
            else if ((squares[1] != 0) && (squares[1] == squares[4]) && (squares[4] == squares[7]))
            {
                isWon = true;
                winner = squares[1];
            } // middle column
            else if ((squares[2] != 0) && (squares[2] == squares[5]) && (squares[5] == squares[8]))
            {
                isWon = true;
                winner = squares[2];
            } // right column
            else if ((squares[0] != 0) && (squares[0] == squares[4]) && (squares[4] == squares[8]))
            {
                isWon = true;
                winner = squares[0];
            } // upper left to bottom right diagonal
            else if ((squares[2] != 0) && (squares[2] == squares[4]) && (squares[4] == squares[6]))
            {
                isWon = true;
                winner = squares[2];
            } // bottom left to upper right diagonal
            else if ((squares[0] != 0) && (squares[1] != 0) && (squares[2] != 0) && (squares[3] != 0) && (squares[4] != 0) && (squares[5] != 0) && (squares[6] != 0) && (squares[7] != 0) && (squares[8] != 0))
            {
                isWon = true;
                winner = 3;
            } // draw
        }
    }
}
