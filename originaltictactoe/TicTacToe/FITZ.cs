﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class FITZV1 : IPlayer
    {
        private int? player;

        public string GetName()
        {
            return "N8 FITZ";
        }

        public Point GetMove(BigBoard bigBoard, int squareLastPlayed)
        {
            // Note: Chooses a random valid move

            if (player == null && squareLastPlayed == -1) player = 1;
            else if (player == null && squareLastPlayed != -1) player = 2;

            int x;
            int y;
            List<int> validSmallBoards = new List<int>();
            List<int> validSquares = new List<int>();



            if (squareLastPlayed != -1)
            {
                x = squareLastPlayed;
            }
            else
            {
                for (int i = 0; i < bigBoard.smallBoards.Length; i++)
                {
                    if (!bigBoard.smallBoards[i].isWon)
                    {
                        validSmallBoards.Add(i);
                    }
                }

                if (validSmallBoards.Contains(4)) x = 4;
                else x = validSmallBoards[0];
            }

            // System.Threading.Thread.Sleep(345);


            for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
            {
                if (bigBoard.smallBoards[x].squares[i] == 0)
                {
                    validSquares.Add(i);
                }
            }

            int best = -1001;
            int bestMove = -1;

            foreach (int i in validSquares)
            {
                var board = new largeBoard(bigBoard, player.Value);
                board.move(new Point(x, i), player.Value);

                int temp = recur(board, squareLastPlayed, 6, player.Value);
                if (temp > best)
                {
                    best = temp;
                    bestMove = i;
                }
            }

            return new Point(x, bestMove);
        }

        private static int recur(largeBoard board, int squareLastPlayed, int depth, int player)
        {
            if (depth == 0) return board.rate(player);

            int bestRate = 0; //I suspect this is one of what are probably many issues with this AI

            if (squareLastPlayed != -1)
            {
                for (int i = 0; i < 9; i++)
                {
                    if (board.boards[squareLastPlayed].squares[i] == 0)
                    {
                        largeBoard tempBoard = board;
                        Point curMove = new Point(squareLastPlayed, i);
                        tempBoard.move(curMove, player);

                        int moveRate = recur(tempBoard, i, depth - 1, (player + 1) % 2 + 1);

                        if (moveRate < bestRate)
                        {
                            bestRate = moveRate;
                        }
                    }
                }
            }
            return bestRate;
        }


        private class littleBoard
        {
            public bool isWon { get; set; }
            public int winner { get; set; }
            public int[] squares;

            public littleBoard(SmallBoard smallBoard)
            {
                squares = new int[9];

                for (int i = 0; i < 9; i++)
                {
                    squares[i] = smallBoard.squares[i];
                }
                isWon = smallBoard.isWon;
                winner = smallBoard.winner;
            }

            public int rate(int player)
            {
                int total = 0;

                int WONBOARD_CONST = 8;


                //IF THE BOARD STATE IS WON
                //rows
                if (squares[0] != 0 && squares[0] == squares[1] && squares[2] == squares[0]) return (player == squares[0]) ? WONBOARD_CONST : WONBOARD_CONST * -1;
                if (squares[3] != 0 && squares[3] == squares[4] && squares[5] == squares[3]) return (player == squares[3]) ? WONBOARD_CONST : WONBOARD_CONST * -1;
                if (squares[6] != 0 && squares[6] == squares[7] && squares[8] == squares[6]) return (player == squares[6]) ? WONBOARD_CONST : WONBOARD_CONST * -1;

                //columns
                if (squares[0] != 0 && squares[0] == squares[3] && squares[6] == squares[0]) return (player == squares[0]) ? WONBOARD_CONST : WONBOARD_CONST * -1;
                if (squares[1] != 0 && squares[1] == squares[4] && squares[7] == squares[1]) return (player == squares[1]) ? WONBOARD_CONST : WONBOARD_CONST * -1;
                if (squares[2] != 0 && squares[2] == squares[5] && squares[8] == squares[2]) return (player == squares[2]) ? WONBOARD_CONST : WONBOARD_CONST * -1;

                //diagonals
                if (squares[0] != 0 && squares[0] == squares[4] && squares[8] == squares[0]) return (player == squares[0]) ? WONBOARD_CONST : WONBOARD_CONST * -1;
                if (squares[2] != 0 && squares[2] == squares[4] && squares[6] == squares[2]) return (player == squares[0]) ? WONBOARD_CONST : WONBOARD_CONST * -1;


                //counting rows
                if (squares[0] != 0 && (squares[1] == 0 || squares[0] == squares[1]) && (squares[2] == 0 || squares[2] == squares[0])) total += (player == squares[0]) ? 1 : -1;
                if (squares[1] != 0 && (squares[0] == 0 || squares[0] == squares[1]) && (squares[2] == 0 || squares[2] == squares[1])) total += (player == squares[0]) ? 1 : -1;
                if (squares[2] != 0 && (squares[0] == 0 || squares[2] == squares[0]) && (squares[1] == 0 || squares[1] == squares[2])) total += (player == squares[0]) ? 1 : -1;

                if (squares[3] != 0 && (squares[4] == 0 || squares[0] == squares[4]) && (squares[5] == 0 || squares[5] == squares[3])) total += (player == squares[0]) ? 1 : -1;
                if (squares[4] != 0 && (squares[3] == 0 || squares[0] == squares[4]) && (squares[5] == 0 || squares[5] == squares[4])) total += (player == squares[0]) ? 1 : -1;
                if (squares[5] != 0 && (squares[3] == 0 || squares[5] == squares[3]) && (squares[4] == 0 || squares[4] == squares[5])) total += (player == squares[0]) ? 1 : -1;

                if (squares[6] != 0 && (squares[7] == 0 || squares[6] == squares[7]) && (squares[8] == 0 || squares[8] == squares[6])) total += (player == squares[0]) ? 1 : -1;
                if (squares[7] != 0 && (squares[6] == 0 || squares[6] == squares[7]) && (squares[8] == 0 || squares[8] == squares[7])) total += (player == squares[0]) ? 1 : -1;
                if (squares[8] != 0 && (squares[6] == 0 || squares[7] == squares[6]) && (squares[7] == 0 || squares[7] == squares[8])) total += (player == squares[0]) ? 1 : -1;


                //counting columns
                if (squares[0] != 0 && (squares[3] == 0 || squares[0] == squares[3]) && (squares[6] == 0 || squares[6] == squares[0])) total += (player == squares[0]) ? 1 : -1;
                if (squares[3] != 0 && (squares[0] == 0 || squares[3] == squares[0]) && (squares[6] == 0 || squares[6] == squares[3])) total += (player == squares[0]) ? 1 : -1;
                if (squares[6] != 0 && (squares[0] == 0 || squares[6] == squares[0]) && (squares[3] == 0 || squares[3] == squares[6])) total += (player == squares[0]) ? 1 : -1;

                if (squares[1] != 0 && (squares[4] == 0 || squares[1] == squares[4]) && (squares[7] == 0 || squares[7] == squares[1])) total += (player == squares[0]) ? 1 : -1;
                if (squares[4] != 0 && (squares[7] == 0 || squares[4] == squares[7]) && (squares[7] == 0 || squares[7] == squares[4])) total += (player == squares[0]) ? 1 : -1;
                if (squares[7] != 0 && (squares[7] == 0 || squares[7] == squares[7]) && (squares[4] == 0 || squares[4] == squares[7])) total += (player == squares[0]) ? 1 : -1;

                if (squares[2] != 0 && (squares[5] == 0 || squares[2] == squares[5]) && (squares[8] == 0 || squares[8] == squares[2])) total += (player == squares[0]) ? 1 : -1;
                if (squares[5] != 0 && (squares[8] == 0 || squares[5] == squares[8]) && (squares[8] == 0 || squares[8] == squares[5])) total += (player == squares[0]) ? 1 : -1;
                if (squares[8] != 0 && (squares[8] == 0 || squares[8] == squares[8]) && (squares[5] == 0 || squares[5] == squares[8])) total += (player == squares[0]) ? 1 : -1;

                //counting diagonals
                if (squares[0] != 0 && (squares[4] == 0 || squares[0] == squares[4]) && (squares[8] == 0 || squares[8] == squares[0])) total += (player == squares[0]) ? 1 : -1;
                if (squares[4] != 0 && (squares[0] == 0 || squares[4] == squares[0]) && (squares[8] == 0 || squares[8] == squares[4])) total += (player == squares[0]) ? 1 : -1;
                if (squares[8] != 0 && (squares[0] == 0 || squares[0] == squares[8]) && (squares[4] == 0 || squares[4] == squares[8])) total += (player == squares[0]) ? 1 : -1;

                if (squares[2] != 0 && (squares[4] == 0 || squares[2] == squares[4]) && (squares[6] == 0 || squares[6] == squares[2])) total += (player == squares[0]) ? 1 : -1;
                if (squares[4] != 0 && (squares[2] == 0 || squares[4] == squares[2]) && (squares[6] == 0 || squares[6] == squares[4])) total += (player == squares[0]) ? 1 : -1;
                if (squares[6] != 0 && (squares[2] == 0 || squares[2] == squares[6]) && (squares[4] == 0 || squares[4] == squares[6])) total += (player == squares[0]) ? 1 : -1;

                return total;
            }
        }

        private class largeBoard
        {
            public bool isWon { get; set; }
            public int winner { get; set; }
            public littleBoard[] boards { get; set; }
            int player { get; set; }


            public largeBoard(BigBoard bigBoard, int player = 0)
            {
                boards = new littleBoard[9];
                for (int i = 0; i < 9; i++)
                {
                    boards[i] = new littleBoard(bigBoard.smallBoards[i]);
                }
                isWon = bigBoard.isWon;
                winner = bigBoard.winner;
            }

            public void move(Point move, int player)
            {
                boards[move.X].squares[move.Y] = player;
            }

            public int rate(int player)//THIS ALSO NEEDS MORE WORK
            {
                int total = 0;
                int board = 0;
                foreach (var lboard in boards)
                {
                    if (board == 4) total += 4 * lboard.rate(player);
                    if (board == 0 || board == 2 || board == 6 || board == 8) total += 3 * lboard.rate(player);
                    if (board == 1 || board == 3 || board == 5 || board == 7) total += 2 * lboard.rate(player);
                    total += lboard.rate(player);
                    board++;
                }

                return total;
            }
        }
    }
}
