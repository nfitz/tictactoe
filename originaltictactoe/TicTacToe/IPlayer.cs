﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public interface IPlayer
    {
        string GetName();
        Point GetMove(BigBoard bigBoard, int squareLastPlayed);
    }
}
