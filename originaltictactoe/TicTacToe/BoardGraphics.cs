﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public class BoardGraphics
    {
        public static Graphics graphics;

        public BoardGraphics(Graphics g)
        {
            graphics = g;
            DrawBoard();
        }

        public static void DrawBoard()
        {
            // draw background
            graphics.FillRectangle(Brushes.LightGray, 50, 50, 450, 450);

            Pen thinGrayLine = new Pen(Brushes.Gray, 2);
            Pen thickBlackLine = new Pen(Brushes.Black, 5);

            // draw thin gray lines across
            for (int i = 0; i < 10; i++)
            {
                graphics.DrawLine(thinGrayLine, 50, 50 + 50 * i, 500, 50 + 50 * i);
            }

            // draw thin gray lines down
            for (int i = 0; i < 10; i++)
            {
                graphics.DrawLine(thinGrayLine, 50 + 50 * i, 50, 50 + 50 * i, 500);
            }

            // draw thick black lines across
            for (int i = 0; i < 4; i++)
            {
                graphics.DrawLine(thickBlackLine, 50, 50 + 150 * i, 500, 50 + 150 * i);
            }

            // draw thick black lines down
            for (int i = 0; i < 4; i++)
            {
                graphics.DrawLine(thickBlackLine, 50 + 150 * i, 50, 50 + 150 * i, 500);
            }
        }

        public static void DrawSymbol(int smallBoard, int? square, int player, string size)
        {
            int x = 50;
            int y = 50;

            // determine starting board for x
            if ((smallBoard == 0) || (smallBoard == 3) || (smallBoard == 6))
            {
                x += 0;
            }
            else if ((smallBoard == 1) || (smallBoard == 4) || (smallBoard == 7))
            {
                x += 150;
            }
            else if ((smallBoard == 2) || (smallBoard == 5) || (smallBoard == 8))
            {
                x += 300;
            }

            // determine starting board for y
            if ((smallBoard == 0) || (smallBoard == 1) || (smallBoard == 2))
            {
                y += 0;
            }
            else if ((smallBoard == 3) || (smallBoard == 4) || (smallBoard == 5))
            {
                y += 150;
            }
            else if ((smallBoard == 6) || (smallBoard == 7) || (smallBoard == 8))
            {
                y += 300;
            }

            // continue to square level if necessary
            if (square.HasValue)
            {
                // determine starting square for x
                if ((square.Value == 0) || (square.Value == 3) || (square.Value == 6))
                {
                    x += 0;
                }
                else if ((square.Value == 1) || (square.Value == 4) || (square.Value == 7))
                {
                    x += 50;
                }
                else if ((square.Value == 2) || (square.Value == 5) || (square.Value == 8))
                {
                    x += 100;
                }

                // determine starting square for y
                if ((square.Value == 0) || (square.Value == 1) || (square.Value == 2))
                {
                    y += 0;
                }
                else if ((square.Value == 3) || (square.Value == 4) || (square.Value == 5))
                {
                    y += 50;
                }
                else if ((square.Value == 6) || (square.Value == 7) || (square.Value == 8))
                {
                    y += 100;
                }
            }

            if (player == 1)
            {
                if (size == "small")
                {
                    Pen pen = new Pen(Brushes.Red, 5);
                    graphics.DrawLine(pen, x + 10, y + 10, x + 40, y + 40);
                    graphics.DrawLine(pen, x + 10, y + 40, x + 40, y + 10);
                    System.Threading.Thread.Sleep(345);
                } // draw small X
                else
                {
                    Pen pen = new Pen(Brushes.DarkRed, 10);
                    graphics.DrawLine(pen, x + 10, y + 10, x + 140, y + 140);
                    graphics.DrawLine(pen, x + 10, y + 140, x + 140, y + 10);
                } // draw big X
            }
            else if (player == 2)
            {
                if (size == "small")
                {
                    graphics.DrawEllipse(new Pen(Brushes.Blue, 5), x + 10, y + 10, 30, 30);
                    System.Threading.Thread.Sleep(345);
                } // draw small O
                else
                {
                    graphics.DrawEllipse(new Pen(Brushes.DarkBlue, 10), x + 10, y + 10, 130, 130);
                } // draw big O
            }
            else if (player == 3)
            {
                if (size == "small")
                {
                    // we'll never get here
                }
                else
                {
                    graphics.DrawString("D", new Font("Arial", 90), Brushes.DarkGreen, x, y);
                } // draw big D
            }
            else
            {
                // an error has occured
            }
        }

        public static void ShowWinner(int winner)
        {
            if (winner == 1)
            {
                MessageBox.Show("X wins!");
            }
            else if (winner == 2)
            {
                MessageBox.Show("O wins!");
            }
            else if (winner == 3)
            {
                MessageBox.Show("Draw!");
            }
            else
            {
                MessageBox.Show("An error has occured.");
            }
        }
    }
}
