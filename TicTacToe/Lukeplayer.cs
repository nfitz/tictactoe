﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class LukeSchlangenPlayer : IPlayer
    {
        public string GetName()
        {
            return "Luke Schlangen 3";
        }

        public Point GetMove(BigBoard bigBoard, int squareLastPlayed)
        {
            #region Set Up

            int x = -1;
            int y = -1;
            List<int> validSmallBoards = new List<int>();
            List<int> validSquares = new List<int>();

            //which player am i?
            int turnsSoFar = 0;
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    if (bigBoard.smallBoards[i].squares[j] > 0)
                    {
                        turnsSoFar += 1;
                    }
                }
            }

            int lukePlayerId = turnsSoFar % 2 + 1;
            int opponentPlayerId;

            int[] bigBoardConversion = new int[9];
            for (int i = 0; i < 9; i++)
            {
                bigBoardConversion[i] = bigBoard.smallBoards[i].winner;
            }

            if (lukePlayerId == 1)
            {
                opponentPlayerId = 2;
            }
            else
            {
                opponentPlayerId = 1;
            }

            List<int> bigBoardWinningMoves = FindWinningMoves(bigBoardConversion, lukePlayerId);
            List<int> opponentBigBoardWinningMoves = FindWinningMoves(bigBoardConversion, opponentPlayerId);

            #endregion
            #region Go For The Win
            if (squareLastPlayed != -1)
            {
                //if you have to play in a specific square
                x = squareLastPlayed;
                //play a winning move for small board as long as it is not a bad move
                bool badMove = false;
                if (opponentBigBoardWinningMoves.Count > 0 && FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId).Count > 0)
                {
                    int opponentWinningMoves = FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId)[0];
                    badMove = opponentBigBoardWinningMoves.Contains(opponentWinningMoves) || bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].isWon;
                }
                if ((bigBoardWinningMoves.Contains(x) || !badMove) && FindWinningMoves(bigBoard.smallBoards[x].squares, lukePlayerId).Count > 0)
                {
                    y = FindWinningMoves(bigBoard.smallBoards[x].squares, lukePlayerId)[0];
                }
            }
            else
            {
                //is it possible to win the game? Then do it.

                if (bigBoardWinningMoves.Count > 0)
                {
                    for (int i = 0; i < bigBoardWinningMoves.Count; i++)
                    {
                        if (FindWinningMoves(bigBoard.smallBoards[bigBoardWinningMoves[i]].squares, lukePlayerId).Count > 0)
                        {
                            x = bigBoardWinningMoves[i];
                            y = FindWinningMoves(bigBoard.smallBoards[bigBoardWinningMoves[i]].squares, lukePlayerId)[0];
                        }
                    }
                }
            }
            #endregion
            #region Block A Win
            if (x < 0 || y < 0)
            {
                if (squareLastPlayed != -1)
                {
                    //if you have to play in a specific square
                    x = squareLastPlayed;
                    //play a winning move if one exists
                    if (opponentBigBoardWinningMoves.Contains(x) && FindWinningMoves(bigBoard.smallBoards[x].squares, lukePlayerId).Count > 0)
                    {
                        y = FindWinningMoves(bigBoard.smallBoards[x].squares, lukePlayerId)[0];
                    }
                    //play blocking move
                    else if (opponentBigBoardWinningMoves.Contains(x) && FindWinningMoves(bigBoard.smallBoards[x].squares, opponentPlayerId).Count > 0)
                    {
                        y = FindWinningMoves(bigBoard.smallBoards[x].squares, opponentPlayerId)[0];
                    }
                }
                else
                {
                    //is it possible to win the game? Then do it.
                    if (opponentBigBoardWinningMoves.Count > 0)
                    {



                        for (int i = 0; i < opponentBigBoardWinningMoves.Count; i++)
                        {
                            bool badMove = false;
                            if (opponentBigBoardWinningMoves.Count > 0 && FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId).Count > 0)
                            {
                                int opponentWinningMoves = FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId)[0];
                                badMove = opponentBigBoardWinningMoves.Contains(opponentWinningMoves) || bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].isWon;
                            }

                            if (FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[i]].squares, lukePlayerId).Count > 0 && !badMove)
                            {
                                x = opponentBigBoardWinningMoves[i];
                                y = FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[i]].squares, lukePlayerId)[0];
                            }
                            else if (FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[i]].squares, opponentPlayerId).Count > 0 && !badMove)
                            {
                                x = opponentBigBoardWinningMoves[i];
                                y = FindWinningMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[i]].squares, opponentPlayerId)[0];
                            }
                        }
                    }
                }
            }
            #endregion

            #region Make A Good Move
            if (squareLastPlayed != -1)
            {
                //if you have to play in a specific square
                x = squareLastPlayed;
                //play a winning move for small board as long as it is not a bad move
                bool badMove = false;
                if (opponentBigBoardWinningMoves.Count > 0 && FindGoodMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId).Count > 0)
                {
                    int opponentWinningMoves = FindGoodMoves(bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].squares, opponentPlayerId)[0];
                    badMove = opponentBigBoardWinningMoves.Contains(opponentWinningMoves) || bigBoard.smallBoards[opponentBigBoardWinningMoves[0]].isWon;
                }
                if (!badMove && FindGoodMoves(bigBoard.smallBoards[x].squares, lukePlayerId).Count > 0)
                {
                    y = FindGoodMoves(bigBoard.smallBoards[x].squares, lukePlayerId)[0];
                }
            }
            else
            {
                //is it possible to win the game? Then do it.

                if (bigBoardWinningMoves.Count > 0)
                {
                    for (int i = 0; i < bigBoardWinningMoves.Count; i++)
                    {
                        if (FindGoodMoves(bigBoard.smallBoards[bigBoardWinningMoves[i]].squares, lukePlayerId).Count > 0)
                        {
                            x = bigBoardWinningMoves[i];
                            y = FindGoodMoves(bigBoard.smallBoards[bigBoardWinningMoves[i]].squares, lukePlayerId)[0];
                        }
                    }
                }
            }
            #endregion
            if (x < 0)
            {
                for (int i = 0; i < bigBoard.smallBoards.Length; i++)
                {
                    if (!bigBoard.smallBoards[i].isWon)
                    {
                        validSmallBoards.Add(i);
                    }
                }
                x = validSmallBoards[new Random(DateTime.Now.Millisecond).Next(0, validSmallBoards.Count)];
            }
            if (y < 0)
            {
                for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
                {
                    if (bigBoard.smallBoards[x].squares[i] == 0 && !opponentBigBoardWinningMoves.Contains(i) && !bigBoard.smallBoards[i].isWon)
                    {
                        validSquares.Add(i);
                    }
                }
                if (validSquares.Count < 1)
                {
                    for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
                    {
                        if (bigBoard.smallBoards[x].squares[i] == 0 && !opponentBigBoardWinningMoves.Contains(i))
                        {
                            validSquares.Add(i);
                        }
                    }
                }
                if (validSquares.Count < 1)
                {
                    for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
                    {
                        if (bigBoard.smallBoards[x].squares[i] == 0)
                        {
                            validSquares.Add(i);
                        }
                    }
                }

                y = validSquares[new Random(DateTime.Now.Millisecond).Next(0, validSquares.Count)];
            }



            return new Point(x, y);
        }


        private List<int> FindWinningMoves(int[] board, int playerId)
        {
            int[,] winningCombinations = new int[,] { { 0, 1, 2 }, { 0, 4, 8 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, { 2, 4, 6 }, { 3, 4, 5 }, { 6, 7, 8 } };
            List<int> winningMoves = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                var winPossible = 0;
                for (int j = 0; j < 3; j++)
                {
                    int k = winningCombinations[i, j];
                    if (board[k] == playerId)
                    {
                        winPossible += 1;
                    }
                    //if the other player is blocking
                    else if (board[k] > 0)
                    {
                        winPossible = 0;
                        break;
                    }
                }
                if (winPossible > 1)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (board[winningCombinations[i, j]] == 0)
                        {
                            winningMoves.Add(winningCombinations[i, j]);
                        }
                    }
                }
            }

            return winningMoves;
        }

        private List<int> FindGoodMoves(int[] board, int playerId)
        {
            int[,] winningCombinations = new int[,] { { 0, 4, 8 }, { 2, 4, 6 }, { 0, 1, 2 }, { 0, 3, 6 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 4, 5 }, { 6, 7, 8 } };
            List<int> winningMoves = new List<int>();
            for (int i = 0; i < 8; i++)
            {
                var winPossible = 0;
                for (int j = 0; j < 3; j++)
                {
                    int k = winningCombinations[i, j];
                    if (board[k] == playerId)
                    {
                        winPossible += 1;
                    }
                    //if the other player is blocking
                    else if (board[k] > 0)
                    {
                        winPossible = 0;
                        break;
                    }
                }
                if (winPossible > 1)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (board[winningCombinations[i, j]] == 0)
                        {
                            winningMoves.Add(winningCombinations[i, j]);
                        }
                    }
                }
            }

            return winningMoves;
        }

    }


}
