﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Game
    {
        // Note: none = 0, player 1 = 1, player 2 = 2, draw = 3
        // Note: If squareLastPlayed is -1 next move can be anywhere
        
        public IPlayer player1 { get; set; }
        public IPlayer player2 { get; set; }
        public BigBoard bigBoard { get; set; }
        public bool isWon { get; set; }
        public int winner { get; set; }
        public int movesMadeSoft { get; set; }
        public int movesMadeHard { get; set; }
        public int playersTurn { get; set; }
        public int squareLastPlayed { get; set; }
        public Point tempPoint1 { get; set; }
        public Point tempPoint2 { get; set; }

        public Game(IPlayer p1, IPlayer p2)
        {
            player1 = p1;
            player2 = p2;
            bigBoard = new BigBoard();
            isWon = false;
            winner = 0;
            movesMadeSoft = 0;
            movesMadeHard = 0;
            playersTurn = 1;
            squareLastPlayed = -1;
            tempPoint1 = new Point();
            tempPoint2 = new Point();
        }

        public int PlayGame()
        {
            while (isWon == false && movesMadeSoft <= 81 && movesMadeHard <= 162)
            {
                if (playersTurn == 1)
                {
                    tempPoint1 = player1.GetMove(bigBoard, squareLastPlayed);
                    if (ValidateMove(squareLastPlayed, tempPoint1.X, tempPoint1.Y))
                    {
                        SaveMove(1, tempPoint1.X, tempPoint1.Y);
                        movesMadeSoft++;
                        movesMadeHard++;
                       // BoardGraphics.DrawSymbol(tempPoint1.X, tempPoint1.Y, 1, "small");
                        CheckForWinner();
                        SetSquareLastPlayed(tempPoint1.Y);
                        playersTurn = 2;
                    }
                    else
                    {
                        movesMadeHard++;
                        squareLastPlayed = -1;
                        playersTurn = 2;
                    }
                }
                else
                {
                    tempPoint2 = player2.GetMove(bigBoard, squareLastPlayed);
                    if (ValidateMove(squareLastPlayed, tempPoint2.X, tempPoint2.Y))
                    {
                        SaveMove(2, tempPoint2.X, tempPoint2.Y);
                        movesMadeSoft++;
                        movesMadeHard++;
                       // BoardGraphics.DrawSymbol(tempPoint2.X, tempPoint2.Y, 2, "small");
                        CheckForWinner();
                        SetSquareLastPlayed(tempPoint2.Y);
                        playersTurn = 1;
                    }
                    else
                    {
                        movesMadeHard++;
                        squareLastPlayed = -1;
                        playersTurn = 1;
                    }
                }
            }
            return winner;
        }

        public bool ValidateMove(int squareLastPlayed, int smallBoard, int square)
        {
            if ((smallBoard < 0) || (smallBoard > 8) || (square < 0) || (square > 8))
            {
                return false;
            } // the move you're trying to make is out of bounds
            else if (bigBoard.smallBoards[smallBoard].squares[square] != 0)
            {
                return false;
            } // the move you're trying to make is already taken
            else if ((squareLastPlayed != -1) && (squareLastPlayed != smallBoard))
            {
                return false;
            } // the move you're trying to make is not on the board corresponding to the square last played
            else
            {
                return true;
            }
        }

        public void SaveMove(int player, int smallBoard, int square)
        {
            bigBoard.smallBoards[smallBoard].squares[square] = player;
        }

        public void CheckForWinner()
        {
            SmallBoard tempSmallBoard = new SmallBoard();
            
            for (int i = 0; i < bigBoard.smallBoards.Length; i++)
            {
                tempSmallBoard = bigBoard.smallBoards[i];
                tempSmallBoard.CheckForWinner();
               // if (tempSmallBoard.isWon) BoardGraphics.DrawSymbol(i, null, tempSmallBoard.winner, "big");
            }

            bigBoard.CheckForWinner();

            isWon = bigBoard.isWon;
            winner = bigBoard.winner;
        }

        public void SetSquareLastPlayed(int square)
        {
            if (bigBoard.smallBoards[square].isWon)
            {
                squareLastPlayed = -1;
            } // the next move can be anywhere
            else
            {
                squareLastPlayed = square;
            } // the next move has to be on the board corresponding to the square last played
        }
    }
}
