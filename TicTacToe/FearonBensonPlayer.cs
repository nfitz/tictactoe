﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class FearonBensonPlayer : IPlayer
    {
        public string GetName()
        {
            return "Fearon Benson";
        }

        public Point GetMove(BigBoard bigBoard, int squareLastPlayed)
        {
            // Note: Chooses a random valid move
            
            int x;
            int y;
            List<int> validSmallBoards = new List<int>();
            List<int> validSquares = new List<int>();

            if (squareLastPlayed != -1)
            {
                x = squareLastPlayed;
            }
            else
            {
                for (int i = 0; i < bigBoard.smallBoards.Length; i++)
                {
                    if (!bigBoard.smallBoards[i].isWon)
                    {
                        validSmallBoards.Add(i);
                    }
                }
                x = validSmallBoards[new Random(DateTime.Now.Millisecond).Next(0, validSmallBoards.Count)];
            }

           // System.Threading.Thread.Sleep(345);

            for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
            {
                if (bigBoard.smallBoards[x].squares[i] == 0)
                {
                    validSquares.Add(i);
                }
            }
            y = validSquares[new Random(DateTime.Now.Millisecond).Next(0, validSquares.Count)];

            return new Point(x, y);
        }
    }
}
