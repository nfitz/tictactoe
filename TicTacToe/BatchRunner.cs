﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    static class BatchRunner
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        static void Main()
        {
            

            IPlayer player1 = new RecursivePlayer(); // enter your AI here
            IPlayer player2 = new FearonBensonPlayer(); //enter opponent AI here

            int played = 0;
            int player1Wins = 0;
            int player2Wins = 0;
            int draws = 0;
            int winner = 0;
            for (int i = 0; i < 1000; i++)
            {

                Game theGame = new Game(player1, player2);
                played++;
                winner = theGame.PlayGame();
                if (winner == 1) player1Wins++;
                if (winner == 2) player2Wins++;
                if(winner == 3) draws++;
            }
            Console.Write(player1Wins + " out of " + played);
            MessageBox.Show(((decimal)player1Wins / played).ToString() + " : " + ((decimal)player2Wins / played).ToString() + " : " + ((decimal) draws/ played));
        }
    }
}
