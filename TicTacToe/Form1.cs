﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form1 : Form
    {
        public IPlayer player1;
        public IPlayer player2;
        public Game game;
        
        public Form1(IPlayer p1, IPlayer p2)
        {
            InitializeComponent();
            player1 = p1;
            player2 = p2;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            // Load
            game = new Game(player1, player2);
            new BoardGraphics(panel1.CreateGraphics());
            label1.Text = "Player 1\n" + "Symbol: X\n" + "Name: " + player1.GetName();
            label2.Text = "Player 2\n" + "Symbol: O\n" + "Name: " + player2.GetName();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Play
            game.PlayGame();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Reset
            game = new Game(player1, player2);
            BoardGraphics.DrawBoard();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Exit
            Application.Exit();
        }
    }
}
