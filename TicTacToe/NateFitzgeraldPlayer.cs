﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class NateFitzgeraldPlayer : IPlayer
    {
        public string GetName()
        {
            return "N8 FITZ";
        }

        public Point GetMove(BigBoard bigBoard, int squareLastPlayed)
        {
            // Note: Chooses a random valid move

            

            int x;
            int y;
            List<int> validSmallBoards = new List<int>();
            List<int> validSquares = new List<int>();

            if (squareLastPlayed != -1)
            {
                x = squareLastPlayed;

            }
            else
            {
                for (int i = 0; i < bigBoard.smallBoards.Length; i++)
                {
                    if (!bigBoard.smallBoards[i].isWon)
                    {
                        validSmallBoards.Add(i);
                    }
                }
                if (validSmallBoards.Contains(4)) x = 4;
                //else if(validSmallBoards.Contains(0)) x = 0;
                //else if (validSmallBoards.Contains(2)) x = 2;
                //else if (validSmallBoards.Contains(6)) x = 6;
                //else if (validSmallBoards.Contains(8)) x = 8;
                else
                {
                    x = validSmallBoards[new Random(DateTime.Now.Millisecond).Next(0, validSmallBoards.Count)];
                }
            }

            // System.Threading.Thread.Sleep(345);


            for (int i = 0; i < bigBoard.smallBoards[x].squares.Length; i++)
            {
                if (bigBoard.smallBoards[x].squares[i] == 0)
                {
                    validSquares.Add(i);
                }
            }
            if (validSquares.Contains(5)) y = 5;
            else
            {
                y = validSquares[new Random(DateTime.Now.Millisecond).Next(0, validSquares.Count)];
            }
            return new Point(x, y);
        }
    }
}
