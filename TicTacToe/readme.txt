﻿As it is currently implemented, the FITZBOT has a recursive minimax algorithm that allows it to play functionally. The chief problem (or at least foremost among many) is it's lack of ability to block opposing wins, every other major component seems to work fine (at least for now).
The todo list in order is as follows:

NON-OPTIONAL
1) Implement blocking awareness
2) The micro rating code is a clusterfuck and there absolutely has to be a better way to do it so I should probably figure that out and write it
3) The macro rating code also (suprise!) sucks big donkey dick, I basically need to clone whatever logic ends up governing selection of small blocks and using it on the macro level but I'm too laxy to do that now, whatever.
4) NEURAL NET! It's easily doable and it should finesse the bot down to a playable state.

OPTIONAL
5) Metadata recorder. We can start with wins and losses but probably should eventually do...
6) Record full games for analysis. XML seems like it would lend itself to the task but who knows?
7) Be able to do something meaningful with recorded full games. Probably at a minimum be able to rotate a board to produce an isomorphic state
8) Record positions and not just games. I have a feeling this would be intense enough to take resources away from our lookahead and that would probably be bad