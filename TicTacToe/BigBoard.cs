﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class BigBoard
    {
        // Note: none = 0, player 1 = 1, player 2 = 2, draw = 3
        
        public bool isWon { get; set; }
        public int winner { get; set; }
        public SmallBoard[] smallBoards { get; set; }

        public BigBoard()
        {
            isWon = false;
            winner = 0;
            smallBoards = new SmallBoard[9];
            for (int i = 0; i < smallBoards.Length; i++)
            {
                smallBoards[i] = new SmallBoard();
            }
        }

        public void CheckForWinner()
        {
            if ((smallBoards[0].isWon) && (smallBoards[0].winner == smallBoards[1].winner) && (smallBoards[1].winner == smallBoards[2].winner))
            {
                isWon = true;
                winner = smallBoards[0].winner;
            } // top row
            else if ((smallBoards[3].isWon) && (smallBoards[3].winner == smallBoards[4].winner) && (smallBoards[4].winner == smallBoards[5].winner))
            {
                isWon = true;
                winner = smallBoards[3].winner;
            } // middle row
            else if ((smallBoards[6].isWon) && (smallBoards[6].winner == smallBoards[7].winner) && (smallBoards[7].winner == smallBoards[8].winner))
            {
                isWon = true;
                winner = smallBoards[6].winner;
            } // bottom row
            else if ((smallBoards[0].isWon) && (smallBoards[0].winner == smallBoards[3].winner) && (smallBoards[3].winner == smallBoards[6].winner))
            {
                isWon = true;
                winner = smallBoards[0].winner;
            } // left column
            else if ((smallBoards[1].isWon) && (smallBoards[1].winner == smallBoards[4].winner) && (smallBoards[4].winner == smallBoards[7].winner))
            {
                isWon = true;
                winner = smallBoards[1].winner;
            } // middle column
            else if ((smallBoards[2].isWon) && (smallBoards[2].winner == smallBoards[5].winner) && (smallBoards[5].winner == smallBoards[8].winner))
            {
                isWon = true;
                winner = smallBoards[2].winner;
            } // right column
            else if ((smallBoards[0].isWon) && (smallBoards[0].winner == smallBoards[4].winner) && (smallBoards[4].winner == smallBoards[8].winner))
            {
                isWon = true;
                winner = smallBoards[0].winner;
            } // upper left to bottom right diagonal
            else if ((smallBoards[2].isWon) && (smallBoards[2].winner == smallBoards[4].winner) && (smallBoards[4].winner == smallBoards[6].winner))
            {
                isWon = true;
                winner = smallBoards[2].winner;
            } // bottom left to upper right diagonal
            else if ((smallBoards[0].isWon) && (smallBoards[1].isWon) && (smallBoards[2].isWon) && (smallBoards[3].isWon) && (smallBoards[4].isWon) && (smallBoards[5].isWon) && (smallBoards[6].isWon) && (smallBoards[7].isWon) && (smallBoards[8].isWon))
            {
                isWon = true;
                winner = 3;
            } // draw
        }
    }
}
